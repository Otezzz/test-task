define([
    'jquery',
    'domReady!'
], function ($) {
    'use strict';

    $.widget('custom.stickyheader', {
        options: {
            headerClass: 'header.page-header',
            topRowClass: '.page-top',
            stickyClass: 'sticky',
            body: 'body'
        },

        _create: function (config, element) {
            this._sticky();
        },

        elemsHeight: function () {
           return {
                headerHeight: $(this.options.headerClass).outerHeight(),
                topRowHeight: $(this.options.topRowClass).outerHeight()
            }
        },

        _sticky: function () {
            var _self = this;

            $(window).scroll(function() {
                ($(this).scrollTop() > _self.elemsHeight().topRowHeight)
                    ? _self._stickUpHeader(_self.options.headerClass, _self.options.body)
                    : _self._unstickUpHeader(_self.options.headerClass, _self.options.body);
            });
        },

        _stickUpHeader: function (header, body) {
            $(header).addClass(this.options.stickyClass);
            $(body).css('padding-top', this.elemsHeight().headerHeight);
        },

        _unstickUpHeader: function (header, body) {
            $(header).removeClass(this.options.stickyClass);
            $(body).css('padding-top', 0);
        }
    });

    return $.custom.stickyheader;
});