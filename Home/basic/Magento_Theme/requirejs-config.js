/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'sticky-header': 'Magento_Theme/js/sticky-header'
        }
    }
};
