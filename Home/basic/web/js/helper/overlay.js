define(['jquery', 'domReady!'], function($) {
    'use strict';

    var overlayHelper = {
        /**
         * Show Overlay
         *
         * @param object visibleElement
         */
        show: function (visibleElementSelector) {
            var that = this;
            // add overlay HTML if no exist
            if(!$('.theme-overlay').length) {
                $('.page-wrapper').append('<div class="theme-overlay"></div>');
            }

            // calculate "not overlapped top path"
            if (typeof visibleElementSelector === 'string' || visibleElementSelector instanceof String) {
                var visibleElement = $(visibleElementSelector);
            }
            if(typeof visibleElement !== 'undefined' && visibleElement.length) {
                that.setTop(visibleElement[0]);
                $(window).on('scroll', function () {
                    that.setTop(visibleElement[0]);
                });
            }

            $('.theme-overlay').show();
            $('.theme-overlay').on('click', function () {
                that.hide();
            });
        },

        hide: function () {
            $('.theme-overlay').css('top', '');
            $('.theme-overlay').hide();
            $('body').removeClass("mobile-body-hidden");
        },

        /**
         * make "required block" visible (not overlapped by overlay)
         *
         * @param {DOM element} visibleElement
         */
        setTop: function (visibleElement) {
            var elPosition = visibleElement.getBoundingClientRect();
            $('.theme-overlay').css('top', elPosition.bottom + 'px');
        }
    };
    return overlayHelper;
});
