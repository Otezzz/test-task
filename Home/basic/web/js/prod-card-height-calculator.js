define([
    'jquery',
    'Magento_Swatches/js/swatch-renderer',
    'domReady!'
     ],
function($, swatch){
    $.widget('custom.card', {

        _init: function () {
            var list = $(this.element).children();
            this.getProductHeight(list);
        },

        getProductHeight: function(products) {
            var height = 0;

            for (var i = 0; i < products.length; i++) {
                var prodBlockHeight = products[i].offsetHeight;

                if (height < prodBlockHeight) {
                    height = prodBlockHeight;
                }
            }
            return this.setProdHeight(products, height);
        },
        setProdHeight: function(products, height) {
            for (var i = 0; i < products.length; i++) {
                $(products[i]).css({'height': height + 'px'});
            }
        }
    });
    return $.custom.card;
});
