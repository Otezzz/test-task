define([
    'jquery',
    'overlay'
    ],
function($, Overlay){
    $.widget('custom.quickView', {

        options: {
            popupClass: ".product-info-popup",
            scrollHeight: 300
        },

        _create: function () {
            let that = this;

            this._on({
                "mouseenter": function () {
                    Overlay.show(this);
                },
                "mouseleave": function () {
                    Overlay.hide();
                }
            });
            $(window).on('scroll', function () {
                if($(document).scrollTop() > that.options.scrollHeight) {
                    that.openPopup();
                } else {
                    that.closeSPopup();
                }
            });
        },

        openPopup: function(popup) {
            $(this.options.popupClass).show('slow');
        },

        closeSPopup: function(popup) {
            $(this.options.popupClass).hide('slow');
        }

    });
    return $.custom.quickView;
});
