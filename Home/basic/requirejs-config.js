var config = {
    map: {
        '*': {
            'overlay': 'js/helper/overlay',
            'quickView': 'js/quick-view-popup',
            'prodCardsHeight': 'js/prod-card-height-calculator'
        }
    }
};
